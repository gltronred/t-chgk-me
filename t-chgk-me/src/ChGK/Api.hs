{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module ChGK.Api
  ( module ChGK.Api
  ) where

import           ChGK.Types

import           Control.Lens
import           Data.Proxy
import           Data.Swagger
import           Data.Time.Clock.POSIX (POSIXTime)
import           Servant.API hiding (Link)
import           Servant.Swagger

type TrustParams a cont
  =  QueryParams "trust-module"    ModuleId
  :> QueryParams "distrust-module" ModuleId
  :> QueryParams "trust-user"    UserId
  :> QueryParams "distrust-user" UserId
  :> QueryFlag   "trust-creator"
  :> QueryFlag   "trust-self-modification"
  :> QueryParams "trust"    (Link a)
  :> QueryParams "distrust" (Link a)
  :> cont

type PlayerApi
  = QueryParam "from" Int :> Get '[JSON] [Player]
  :<|> Capture "playerId" PlayerId :> Get '[JSON] Player
  :<|> Capture "playerId" PlayerId :> "history" :> TrustParams PlayerId (Get '[JSON] [PlayerAtom])
  :<|> "history" :> TrustParams PlayerId (Get '[JSON] [PlayerAtom])
  -- POST
  :<|> ReqBody '[JSON] PlayerPost :> Post '[JSON] Player
  :<|> Capture "playerId" PlayerId :> ReqBody '[JSON] (Diff PlayerId PlayerAtom) :> Post '[JSON] Player

type TournamentApi
  =    QueryParam "from" Int :> Get '[JSON] (Listing Tournament)
  :<|> Capture "tournamentId" TournamentId :> QueryParam "from" Int :> Get '[JSON] (Listing TeamTournament)
  :<|> Capture "tournamentId" TournamentId :> "teams" :> Capture "teamId" TeamId :> Get '[JSON] TeamTournament
  :<|> Capture "tournamentId" TournamentId :> "questions" :> Capture "question" Int :> Get '[JSON] QuestionTournament
  :<|> "updates" :> QueryParam "since" POSIXTime :> TrustParams TournamentId (Get '[JSON] UpdateList)
  -- POST
  :<|> ReqBody '[JSON] TournamentInfo :> Post '[JSON] Tournament
  :<|> Capture "tournamentId" TournamentId :> ReqBody '[JSON] (Diff TournamentId TournamentAtom) :> Post '[JSON] Tournament

type TApi
  =      "players" :> PlayerApi
    :<|> "tournaments" :> TournamentApi

tApiDesc :: Swagger
tApiDesc = toSwagger (Proxy :: Proxy TApi)
  & info.title .~ "API ЦТС"
  & info.version .~ "0.2"
  & info.description ?~ "API для работы с центральным турнирным сайтом."
  & info.license ?~ "MIT"
  & info.contact ?~ (mempty
                     & name ?~ "Мансур Зиятдинов"
                     & email ?~ "chgk@pm.me"
                    )
  & host ?~ "t.chgk.me"
  & basePath ?~ "/api/v0.2.1"
  -- & securityDefinitions .~ singleton
  -- { _securitySchemeType = SecuritySchemeBasic
  -- , _securitySchemeDescription = Just "All GET methods are public, meaning that you can read all the data. Write operations require authentication and therefore are forbidden to the general public."
  -- }

