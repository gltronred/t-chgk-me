{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StrictData #-}

module ChGK.Types.Team
  ( Atom (..)
  , TeamId (..)
  , Team (..)
  ) where

import ChGK.Types.Meta
import ChGK.Types.Opts

import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Text (Text)
import GHC.Generics

data Atom
  = Alias { _site :: Text, _ident :: Value }
  | TeamName { _name :: Text }
  | TeamTown { _town :: Text }
  | Other Value
  deriving (Eq,Show,Read,Generic)

instance FromJSON Atom
instance ToJSON Atom where
  toEncoding = genericToEncoding $ jsonOpts 0

newtype TeamId = TeamId { unTeamId :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

instance ToSchema TeamId where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)
instance ToParamSchema TeamId

data Team = Team
  { teamId :: TeamId
  , teamInfo :: [Diff TeamId Atom]
  } deriving (Eq,Show,Read,Generic)



