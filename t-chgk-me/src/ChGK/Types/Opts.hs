{-# LANGUAGE TypeFamilies #-}

module ChGK.Types.Opts
  ( jsonOpts
  , schemaOpts
  , describeProp
  ) where

import           Control.Lens
import           Data.Aeson (camelTo2, Options)
import qualified Data.Aeson as Aeson
import           Data.Swagger (SchemaOptions, Schema, Referenced, ToSchema, HasSchema, HasProperties)
import qualified Data.Swagger as Swagger
import           Data.Text (Text)

jsonOpts :: Int -> Options
jsonOpts k = Aeson.defaultOptions
  { Aeson.fieldLabelModifier = camelTo2 '-' . drop k
  , Aeson.constructorTagModifier = camelTo2 '-'
  , Aeson.unwrapUnaryRecords = True
  }

schemaOpts :: Int -> SchemaOptions
schemaOpts k = Swagger.defaultSchemaOptions
  { Swagger.fieldLabelModifier = camelTo2 '-' . drop k
  , Swagger.constructorTagModifier = camelTo2 '-'
  , Swagger.unwrapUnaryRecords = True
  }

inlineSchemaDesc :: ToSchema p => proxy p -> Text -> Referenced Schema
inlineSchemaDesc p t = Swagger.Inline $ Swagger.toSchema p & Swagger.description ?~ t

describeProp :: (Functor f, ToSchema p, HasSchema b a1, HasProperties a1 a2, At a2, IxValue a2 ~ Referenced Schema)
             => Index a2
             -> proxy p
             -> Text
             -> f b
             -> f b
describeProp prop proxy text a = a & mapped .Swagger.schema . Swagger.properties . at prop ?~ inlineSchemaDesc proxy text

