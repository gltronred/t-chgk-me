{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module ChGK.Types.Player
  ( module ChGK.Types.Player
  ) where

import ChGK.Types.Common
import ChGK.Types.Meta
import ChGK.Types.Opts

import Control.Lens
import Data.Aeson
import Data.Swagger
import Data.Proxy
import Data.Text (Text)
import Data.Time (Day)
import GHC.Generics (Generic)

data Fullname = FullnameC
  { surname :: Text
  , name :: Maybe Text
  , patronym :: Maybe Text
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Fullname
instance ToJSON Fullname
instance ToSchema Fullname where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Содержит обязательное поле surname (фамилия) и опциональные name (имя) и patronym (отчество). Если у игрока невозможно выделить имя и/или отчество, имя целиком вводится в поле surname"

data PlayerAtom
  = Alias Alias
  | Fullname Fullname
  | Birthdate Day
  | Other Extra
  deriving (Eq,Show,Read,Generic)

instance FromJSON PlayerAtom where
  parseJSON = genericParseJSON $ jsonOpts 0
instance ToJSON PlayerAtom where
  toEncoding = genericToEncoding $ jsonOpts 0
  toJSON = genericToJSON $ jsonOpts 0
instance ToSchema PlayerAtom where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.title ?~ "Player.Atom"
    & mapped.schema.description ?~ "Атом изменения (diff) информации об игроке. В объекте может быть только одно поле из перечисленных в описании, каждое поле задаёт возможное изменение. Поле alias задаёт связь с идентификаторами на других сайтах. Поле other - дополнительную информацию об игроке (на усмотрение ТС). Остальные поля соответствуют полям Player"

newtype PlayerDiff = PlayerDiff (Diff PlayerId PlayerAtom) deriving (Eq,Show,Read,Generic)
instance ToSchema (Diff PlayerId PlayerAtom) where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 0
instance ToSchema PlayerDiff where
  declareNamedSchema _ = genericDeclareNamedSchema (schemaOpts 0) (Proxy :: Proxy (Diff PlayerId PlayerAtom))
    & mapped.schema.title ?~ "Player.Diff"
    & mapped.schema.description ?~ "Изменение (diff) информации об игроке. В обекте может быть только одно поле из перечисленных в описании, каждое поле задаёт возможное изменение. Поле diff задаёт изменение информации. Поле vote - указание на корректность/некорректность с точки зрения автора информации, введённой ранее. Поле merge - сливает информацию другого игрока на ЦТС с данным."

newtype PlayerId = PlayerId { unPlayerId :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

instance ToSchema PlayerId where
  declareNamedSchema _p = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор игрока"
instance ToParamSchema PlayerId

data PlayerPost = PlayerPost
  { ppFullname :: Fullname
  , ppBirthdate :: Maybe Day
  } deriving (Eq,Show,Read,Generic)

instance FromJSON PlayerPost where
  parseJSON = genericParseJSON $ jsonOpts 2
instance ToSchema PlayerPost where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 2) p
    & mapped.schema.description ?~ "Начальная информация об игроке: его ФИО и, опционально, дата рождения"

data Player = Player
  { playerId :: PlayerId
  , playerRootId :: Maybe PlayerId
  , playerFullname :: Fullname
  , playerBirthdate :: Maybe Day
  , playerAliases :: [Alias]
  , playerOther :: [Extra]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON Player where
  toJSON = genericToJSON $ jsonOpts 6
  toEncoding = genericToEncoding $ jsonOpts 6
instance ToSchema Player where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 6) p
    & mapped.schema.description ?~ "Аккумулированная информация об игроке. Поля id и root-id содержат идентификаторы игрока и \"корня\" (если игрок был слит с другими игроками на ЦТС, изменения нужно будет вносить в корневого игрока). Поля fullname, birthday, other содержат, соответственно, ФИО, дату рождения и дополнительную информацию об игроке. Поле aliases содержит идентификаторы игрока на других ТС"

