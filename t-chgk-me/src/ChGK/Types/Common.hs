{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module ChGK.Types.Common
  ( Alias (..)
  , Extra (..)
  , Listing (..)
  ) where

import ChGK.Types.Opts

import Control.Lens
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Text (Text)
import GHC.Generics

data Alias = MkAlias
  { site :: Text
  , ident :: Object
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Alias
instance ToJSON Alias
instance ToSchema Alias where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Идентификатор на другом ТС. Поле site содержит идентификатор сайта"
    & describeProp "site"  (Proxy :: Proxy Text)   "Идентификатор сайта (реестр ведётся КР)"
    & describeProp "ident" (Proxy :: Proxy Object) "Идентификатор на другом ТС"

data Extra = MkExtra
  { atomType :: Text
  , atomValue :: Object
  } deriving (Eq,Show,Read,Generic)

instance FromJSON Extra where
  parseJSON = genericParseJSON $ jsonOpts 4
instance ToJSON Extra where
  toJSON = genericToJSON $ jsonOpts 4
  toEncoding = genericToEncoding $ jsonOpts 4
instance ToSchema Extra where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 4) p
    & mapped.schema.description ?~ "Дополнительная информация"
    & describeProp "type"  (Proxy :: Proxy Text)   "Название типа дополнительной информации"
    & describeProp "value" (Proxy :: Proxy Object) "Значение дополнительной информации"

data Listing a = Listing
  { lstTotal :: Int
  , lstItems :: [a]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON a => ToJSON (Listing a) where
  toJSON = genericToJSON $ jsonOpts 3
  toEncoding = genericToEncoding $ jsonOpts 3
instance ToSchema a => ToSchema (Listing a) where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 3) p
    & mapped.schema.description ?~ "Список сущностей"
    & describeProp "total" (Proxy :: Proxy Int) "Общее количество сущностей"
    -- & describeProp "items" (Proxy :: Proxy [a]) "Сущности, которые выбраны параметрами"

