{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}

module ChGK.Types.Tournament
  ( module ChGK.Types.Tournament
  ) where

import ChGK.Types.Author
import ChGK.Types.Common
import ChGK.Types.Link
import ChGK.Types.Meta
import ChGK.Types.Opts
import ChGK.Types.Player (PlayerId)
import ChGK.Types.Team (TeamId)

import Control.Lens
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Time.Clock.POSIX (POSIXTime)
import GHC.Generics

data TournamentType
  = Synchronous                 -- ^ синхронный
  | StrictlySynchronous         -- ^ строго синхронный
  | InPerson                    -- ^ очный
  | OverallClassification       -- ^ общий зачет
  | Asynchronous                -- ^ асинхронный
  deriving (Eq,Show,Read,Generic)

instance FromJSON TournamentType where
  parseJSON = genericParseJSON $ jsonOpts 0
instance ToJSON TournamentType where
  toEncoding = genericToEncoding $ jsonOpts 0
  toJSON = genericToJSON $ jsonOpts 0
instance ToSchema TournamentType where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Тип турнира: синхронный, строго синхронный, очный, общий зачёт, асинхронный"

data TournamentDateType
  = RequestsAllowedBefore
  | DownloadingQuestions
  | ReceivingRecaps
  | ReceivingResults
  | ReceivingAppeals
  | ControversialResults
  | AppealResults
  | PaymentDue
  deriving (Eq,Show,Read,Generic)

instance ToJSON TournamentDateType where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0
instance ToSchema TournamentDateType where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Даты в турнире: прием заявок, скачивание вопросов, ввод составов, ввод результатов, ввод апелляций, результаты спорных, результаты апелляций, оплата турнира"

data TournamentDate = TournamentDate
  { dateType :: TournamentDateType
  , dateStart :: Maybe UTCTime
  , dateEnd :: Maybe UTCTime
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TournamentDate where
  toJSON = genericToJSON $ jsonOpts 4
  toEncoding = genericToEncoding $ jsonOpts 4
instance ToSchema TournamentDate where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 4) p
    & mapped.schema.description ?~ "Диапазон дат в турнире. Поле type содержит тип, опциональные поля start и end - начало и конец диапазона (их значения по умолчанию - начало времён и произвольный момент в будущем, соответственно)"

data PaymentCategory = PaymentCategory
  { size :: Text
  , category :: Maybe Text
  } deriving (Eq,Show,Read,Generic)

instance ToJSON PaymentCategory where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0
instance ToSchema PaymentCategory where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Описание оплаты. Поле size содержит размер оплаты, опциональное поле category - категорию команд (если не указано - все команды)"

data TournamentInfo = TournamentInfo
  { tiName :: Text
  , tiOrganizers :: [UserId]
  , tiGameJury :: [UserId]
  , tiAppealJury :: [UserId]
  , tiTourFormula :: [Int]
  , tiType :: TournamentType
  , tiPayment :: [PaymentCategory]
  , tiDates :: [TournamentDate]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TournamentInfo where
  toJSON = genericToJSON $ jsonOpts 2
  toEncoding = genericToEncoding $ jsonOpts 2
instance ToSchema TournamentInfo where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 2) p
    & mapped.schema.description ?~ "Информация о турнире. Поля: name - название турнира, organizers - список ОК, game-jury - список ИЖ, appeal-jury - список АЖ, tour-formula - список с количеством вопросов по турам, type - тип турнира, payment - список с описанием оплаты по категориям команд (в нём должно быть не более одного элемента с неуказанной категорией), dates - список с датами в турнире"

newtype TournamentDiff = TournamentDiff (Diff TournamentId TournamentAtom) deriving (Eq,Show,Read,Generic)
instance ToSchema (Diff TournamentId TournamentAtom) where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 0
instance ToSchema TournamentDiff where
  declareNamedSchema _ = genericDeclareNamedSchema (schemaOpts 0) (Proxy :: Proxy (Diff TournamentId TournamentAtom))
    & mapped.schema.title ?~ "Tournament.Diff"
    & mapped.schema.description ?~ "Изменение (diff) информации о турнире. В обекте может быть только одно поле из перечисленных в описании, каждое поле задаёт возможное изменение. Поле diff задаёт изменение информации. Поле vote - указание на корректность/некорректность с точки зрения автора информации, введённой ранее. Поле merge - сливает информацию другого турнира на ЦТС с данным."

newtype TournamentId = TournamentId { unTournamentId :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

instance ToSchema TournamentId where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор турнира"
instance ToParamSchema TournamentId

data QuestionResult = QuestionResult
  { qvTime :: Maybe Double
  , qvValue :: Bool
  } deriving (Eq,Show,Read,Generic)

instance ToJSON QuestionResult where
  toEncoding = genericToEncoding $ jsonOpts 2
  toJSON = genericToJSON $ jsonOpts 2
instance ToSchema QuestionResult where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 2

data TeamResult = TeamResult
  { trQuestion :: Int
  , trResult :: QuestionResult
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TeamResult where
  toEncoding = genericToEncoding $ jsonOpts 2
  toJSON = genericToJSON $ jsonOpts 2
instance ToSchema TeamResult where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 2

data Change = Change
  { teamId :: Maybe TeamId
  , question :: Maybe Int
  , value :: QuestionResult
  } deriving (Eq,Show,Read,Generic)

instance ToJSON Change where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0
instance ToSchema Change where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Описание изменения в таблице. Опциональное поле team-id содержит номер команды, question - номер вопроса (если не указаны, изменение относится ко всем командам или ко всем вопросам, соответственно; может не быть указано только одно поле из двух). Поле value содержит результат, записываемый в выбранные поля таблицы"

data QuestionTournament = QuestionTournament
  { qtTeamId :: TeamId
  , qtResult :: QuestionResult
  } deriving (Eq,Show,Read,Generic)

instance ToJSON QuestionTournament where
  toEncoding = genericToEncoding $ jsonOpts 2
  toJSON = genericToJSON $ jsonOpts 2
instance ToSchema QuestionTournament where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 2

data TeamTournament = TeamTournament
  { ttTeamId :: TeamId
  , ttName :: Text
  , ttTown :: Maybe Text
  , ttRecap :: [PlayerId]
  , ttResults :: [TeamResult]
  } deriving (Eq,Show,Read,Generic)

instance ToJSON TeamTournament where
  toEncoding = genericToEncoding $ jsonOpts 2
  toJSON = genericToJSON $ jsonOpts 2
instance ToSchema TeamTournament where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 2

data Tournament = Tournament
  { tournamentId :: TournamentId
  , tournamentInfo :: TournamentInfo
  , tournamentParticipants :: Int
  } deriving (Eq,Show,Read,Generic)

instance ToJSON Tournament where
  toJSON = genericToJSON $ jsonOpts 10
  toEncoding = genericToEncoding $ jsonOpts 10
instance ToSchema Tournament where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 10

data TournamentAtom
  = Alias Alias
  | Name Text
  | Organizers [UserId]
  | GameJury [UserId]
  | AppealJury [UserId]
  | Request { _representative :: UserId, _narrator :: UserId, _town :: Text, _time :: UTCTime }
  | TourFormula [Int]
  | HasRating Bool
  | TournamentType TournamentType
  | Payment [PaymentCategory]
  | Dates [TournamentDate]
  | Participant { _teamName :: Text, _teamTown :: Maybe Text, _recap :: [PlayerId] }
  | Input { _input :: [Change] }
  | ControversialResult
    { _input :: [Change]
    , _controversial :: Link Tournament
    , _verdict :: Maybe Text
    }
  | Appeal Text
  | AppealResult
    { _input :: [Change]
    , _appeal :: Link Tournament
    , _verdict :: Maybe Text
    }
  | Disqualify
    { _input :: [Change]
    , _reason :: Text
    }
  | Other Extra
  deriving (Eq,Show,Read,Generic)
makeLenses ''TournamentAtom
makePrisms ''TournamentAtom

instance ToJSON TournamentAtom where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0
instance ToSchema TournamentAtom where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.title ?~ "Tournament.Atom"
    & mapped.schema.description ?~ "Атом изменения (diff) информации о турнире. В объекте может быть только одно поле из перечисленных в описании, каждое поле задаёт возможное изменение. Поле alias задаёт связь с идентификаторами на других сайтах. Поле other - дополнительную информацию о турнире (на усмотрение ТС). Остальные поля соответствуют полям Tournament"

data UpdateList = UpdateList
  { ulLastUpdate :: POSIXTime
  , ulItems :: [Diff TournamentId TournamentAtom]
  } deriving (Eq,Show,Generic)

instance ToJSON UpdateList where
  toEncoding = genericToEncoding $ jsonOpts 2
  toJSON = genericToJSON $ jsonOpts 2
instance ToSchema UpdateList where
  declareNamedSchema = genericDeclareNamedSchema $ schemaOpts 2

