{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module ChGK.Types.Link
  ( Link(..)
  ) where

import Control.Lens
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Text (Text)
import GHC.Generics

newtype Link a = Link { unLink :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)
instance ToSchema (Link a) where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Ссылка на сущность"
instance ToParamSchema (Link a)

