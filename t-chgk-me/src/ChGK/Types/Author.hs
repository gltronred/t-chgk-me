{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module ChGK.Types.Author
  ( ModuleId (..)
  , UserId (..)
  , AuthorId (..)
  ) where

import ChGK.Types.Opts

import Control.Lens
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Text
import GHC.Generics

newtype ModuleId = ModuleId { unModuleId :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)
instance ToSchema ModuleId where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор модуля"
instance ToParamSchema ModuleId

newtype UserId = UserId { unUserId :: Text }
  deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)
instance ToSchema UserId where
  declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор пользователя"
instance ToParamSchema UserId

data AuthorId = AuthorId
  { userId :: UserId
  , moduleId :: Maybe ModuleId
  } deriving (Eq,Show,Read,Generic)
instance ToJSON AuthorId where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0
instance ToSchema AuthorId where
  declareNamedSchema p = genericDeclareNamedSchema (schemaOpts 0) p
    & mapped.schema.description ?~ "Кем создана информация"

