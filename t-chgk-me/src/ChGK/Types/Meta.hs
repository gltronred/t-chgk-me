{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}

module ChGK.Types.Meta
  ( Diff (..)
  , link
  , author
  , update
  , isValid
  , entity
  , _Diff
  , _Vote
  , _Merge
  ) where

import ChGK.Types.Author
import ChGK.Types.Link
import ChGK.Types.Opts

import Control.Lens.TH
import Data.Aeson
import GHC.Generics (Generic)

data Diff ident update
  = Diff { _link :: Link ident
         , _author :: AuthorId
         , _update :: update
         }
  | Vote { _link :: Link ident
         , _author :: AuthorId
         , _isValid :: Bool
         }
  | Merge { _entity :: ident
          , _author :: AuthorId
          }
  deriving (Eq,Show,Read,Generic)
makeLenses ''Diff
makePrisms ''Diff

instance (ToJSON ident, ToJSON update) => ToJSON (Diff ident update) where
  toJSON = genericToJSON $ jsonOpts 0
  toEncoding = genericToEncoding $ jsonOpts 0

