module ChGK.Lib
  ( module ChGK.Lib
  ) where

import           ChGK.Api

import           Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as BSL8

swaggerMain :: IO ()
swaggerMain = BSL8.putStrLn $ encode tApiDesc

