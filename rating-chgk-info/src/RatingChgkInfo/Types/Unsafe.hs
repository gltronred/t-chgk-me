{-|
Module      : RatingChgkInfo.Types.Unsafe
Description : Небезопасные функции (в смысле безопасности типов)
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

Из этого модуля экспортируются функции, которые не являются безопасными с точки зрения типов.

Например, можно ошибиться и передать идентификатор игрока вместо идентификатора
команды, если оба идентификатора имеют текстовый тип. Чтобы избежать этого
класса ошибок мы используем разные типы для разных идентификаторов, а получить
значения-идентификаторы можно только из запросов к API. Ну, или если хочется
иметь возможность выстрелить в ногу — из этого модуля. Caveat emptor
-}

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module RatingChgkInfo.Types.Unsafe
  ( -- * Типы-идентификаторы
    PlayerId (..)
  , TeamId (..)
  , TournamentId (..)
  ) where

import Control.Lens
import Data.Aeson
import Data.Swagger (ToSchema, declareNamedSchema, schema, description, ToParamSchema)
import Data.Text (Text)
import Servant.API

-- | Идентификатор игрока. В API на самом деле возвращается строка, но во всех
-- функциях работы с игроками используется этот тип. Чтобы получить значение
-- этого типа следует получить данные из API
newtype PlayerId = PlayerId
  { unPlayerId :: Text
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,FromHttpApiData,ToHttpApiData)

instance ToSchema PlayerId where
  declareNamedSchema _p = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор игрока"
instance ToParamSchema PlayerId

-- | Идентификатор команды. В API на самом деле возвращается строка, но во всех
-- функциях работы с командами используется этот тип. Чтобы получить значение
-- этого типа следует получить данные из API
newtype TeamId = TeamId
  { unTeamId :: Text
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,FromHttpApiData,ToHttpApiData)

instance ToSchema TeamId where
  declareNamedSchema _p = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор игрока"
instance ToParamSchema TeamId

-- | Идентификатор турнира. В API на самом деле возвращается строка, но во всех
-- функциях работы с турнирами используется этот тип. Чтобы получить значение
-- этого типа следует получить данные из API
newtype TournamentId = TournamentId
  { unTournamentId :: Text
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,FromHttpApiData,ToHttpApiData)

instance ToSchema TournamentId where
  declareNamedSchema _p = declareNamedSchema (Proxy :: Proxy Text)
    & mapped.schema.description ?~ "Идентификатор игрока"
instance ToParamSchema TournamentId

-- | Идентификатор релиза. В API на самом деле возвращается строка, но во всех
-- функциях работы с релизами используется этот тип. Чтобы получить значение
-- этого типа следует получить данные из API
newtype ReleaseId = ReleaseId
  { unReleaseId :: Text
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,ToHttpApiData)

-- | Идентификатор сезона. В API на самом деле возвращается строка, но во всех
-- функциях работы с сезонами используется этот тип. Чтобы получить значение
-- этого типа следует получить данные из API
newtype SeasonId = SeasonId
  { unSeasonId :: Text
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,ToHttpApiData)

-- TODO: TownId, RegionId, CountryId

