# rating-chgk-info

[![Hackage](https://img.shields.io/hackage/v/rating-chgk-info.svg)](https://hackage.haskell.org/package/rating-chgk-info)
[![BSD3 license](https://img.shields.io/badge/license-BSD3-blue.svg)](LICENSE)

Client for rating.chgk.info API and CSV tables

[Документация](https://doc.chgk.me/rating-chgk-info/)

Клиент для REST API сайта рейтинга (rating.chgk.info) и функциональности, которой нет в REST API, но которая доступна через экспорт CSV.
Также содержит REST-сервер для дополнительной функциональности, доступной через CSV.

Сгенерированные JS-функции для работы с REST API: https://bitbucket.org/gltronred/t-chgk-me/downloads/rating-chgk-info-0.2.1.js

В следующем большом релизе планируется заменить в части типов для REST API списки значений на множества (Set), например, для составов команд и т.п. Это должно повысить безопасность библиотеки (сложнее будет выстрелить себе в ногу), и не должно ухудшить возможности работы.

## Установка и запуск

Для работы с библиотекой необходимо добавить зависимость `rating-chgk-info`.

Запустить пример можно при помощи `cabal new-run example1`.

## Пример использования:

Больше примеров находятся в папке examples

```haskell
-- Немного наших библиотек
import RatingChgkInfo

-- И немного стандартных библиотек
import Control.Monad (forM, void)
import Control.Monad.IO.Class (liftIO)
import Data.List (nub)
import Data.Time (LocalTime(..),fromGregorian,midnight)

-- Точка входа в приложение
main :: IO ()

-- Функция runRatingApi запускает работу клиента, это позволяет разделять эффекты
main = void $ runRatingApi $ do

  -- Получим список всех очных турниров за 2018 год

  let s2018 = LocalTime (fromGregorian 2018 1 1) midnight
      e2018 = LocalTime (fromGregorian 2019 1 1) midnight

  -- Функция tournaments получает одну страницу турниров, а функция getAllItems
  -- оборачивает подобные функции, чтобы пройтись по всем страницам.
  -- Далее из этого списка выбираются очные турниры 2018 года
  tourns <- filter (\t -> trs_typeName t == Casual &&
                          trs_dateStart t >= s2018 &&
                          trs_dateEnd t <= e2018) <$>
            getAllItems tournaments

  -- Проходимся по полученному списку
  ts <- forM tourns $ \TournamentShort{trs_idtournament = ident} -> do

    -- Получаем результаты турнира
    res <- tournamentResults ident

    -- Возвращаем названия команд-участниц
    pure (map tr_current_name res)

  -- Выводим, сколько уникальных названий было по всем турнирам
  liftIO (print (length (nub ts)))

```
