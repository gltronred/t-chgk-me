{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}

import           RatingChgkInfo
import           RatingChgkInfo.Types.Unsafe (TournamentId (..), PlayerId (..))

import           Data.Aeson
import           Data.Cache (Cache)
import qualified Data.Cache as C
import           Data.Hashable
import           Data.List (lookup)
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import           Data.Time
import           System.Directory (withCurrentDirectory)
import           System.Environment (getArgs)

deriving instance Hashable TournamentId

main :: IO ()
main = do
  args <- getArgs
  cache <- C.newCache Nothing
  case args of
    [rootDir, "town", townStr, town] -> withCurrentDirectory rootDir $ 
      case readMaybe townStr of
        Nothing -> usage
        Just townId -> workTown cache townId (T.pack town) Nothing >>= mapM_ print
    [rootDir, "calendar"] -> withCurrentDirectory rootDir workTournaments
    [rootDir, townPagesStr] -> withCurrentDirectory rootDir $ 
      case readMaybe townPagesStr of
        Nothing -> usage
        Just townPages -> work cache townPages
    _ -> usage

usage :: IO ()
usage = do
  die "Usage: calendar-rating <rootFolder> <townPages> | <rootFolder> calendar"

cachedTournament :: Cache TournamentId Tournament -> TournamentId -> IO (Either String Tournament)
cachedTournament cache ident = do
  mcached <- C.lookup cache ident
  case mcached of
    Just cached -> pure $ Right cached
    Nothing -> do
      elive <- runRatingApi $ tournament ident
      case elive of
        Left err -> do
          T.putStrLn $ show err
          pure $ Left $ show err
        Right [live] -> do
          C.insert cache ident live
          pure $ Right live
        Right _ -> do
          T.putStrLn "Something went wrong with tournament API"
          pure $ Left "Something went wrong with tournament"

work :: Cache TournamentId Tournament -> Int -> IO ()
work cache townPages = do
  ets <- fmap (fmap concat . sequence) $ forM [1..townPages] $ towns . Just
  case ets of
    Left err -> T.putStrLn $ decodeUtf8 err
    Right ts -> do
      ss <- forM ts $ \Town{ townId = ident, townName = town, townOtherName = other } -> do
        T.putStrLn town
        workTown cache ident town other
      encodeFile "names.json" $
        map fst $
        filter (not . null . snd) $
        zip ts ss

workTown :: Cache TournamentId Tournament -> Int -> Text -> Maybe Text -> IO [SynchTown]
workTown cache ident town other = do
  now <- getCurrentTime
  ests <- synchTown ident
  case ests of
    Left err -> T.putStrLn (decodeUtf8 err) >> pure []
    Right sts -> do
      let fname = "t" ++ show ident ++ ".ics"
      let events = filter ((==ClaimAccepted) . stStatus) sts
      let toInt = fromMaybe 36 . readMaybe . T.unpack . fromMaybe "36"
      eventTourns <- forM events $ \ev -> (ev,) . either (const 36) (toInt . trn_questionsTotal)
                                          <$> cachedTournament cache (stTournamentId ev)
      unless (null events) $
        T.writeFile fname $ T.intercalate "\r\n"
        [ calStart
        , T.intercalate "\r\n" $
          map (\(ev,questions) -> mkEvent now town other questions ev) eventTourns
        , calEnd
        ]
      pure events

calStart :: Text
calStart = T.intercalate "\r\n"
  [ "BEGIN:VCALENDAR"
  , "VERSION:2.0"
  , "PRODID:-//calendar.chgk.me/EN"
  , "METHOD:PUBLISH"
  ]

calEnd :: Text
calEnd = "END:VCALENDAR"

-- | addLocalTime a b = a + b
addLocalTime :: NominalDiffTime -> LocalTime -> LocalTime
addLocalTime x = utcToLocalTime utc . addUTCTime x . localTimeToUTC utc

mkEvent :: UTCTime -> Text -> Maybe Text -> Int -> SynchTown -> Text
mkEvent now town other questions
  SynchTown{ stTournamentId = TournamentId tid
           , stTournament = tourn
           , stRepresentativeId = PlayerId rid
           , stRepresentative = rep
           , stTime = time }
  = T.intercalate "\r\n"
    [ "BEGIN:VEVENT"
    , "UID:" `T.append` uid
    , "DTSTAMP:" `T.append` ts
    , "DTSTART;TZID=Europe/Moscow:" `T.append` start
    , "DTEND;TZID=Europe/Moscow:" `T.append` end
    , "SUMMARY:" `T.append` summary
    , "ORGANIZER:" `T.append` org
    , "LOCATION:" `T.append` loc
    , "URL:" `T.append` url
    , "DESCRIPTION:" `T.append` desc
    , "END:VEVENT"
    ]
  where
    uid = T.concat [ "t", tid, "-r", rid, "@calendar.chgk.me" ]
    format :: FormatTime t => t -> Text
    format = T.pack . formatTime defaultTimeLocale "%Y%m%dT%H%M%S"
    ts = format now `T.append` "Z"
    start = format time
    end = format $ duration `addLocalTime` time
    duration = (3600 :: NominalDiffTime) * fromIntegral questions / 15
    summary = tourn
    org = rep
    loc = T.concat $ town : maybe [] pure other
    url = T.concat [ "https://rating.chgk.info/tournament/", tid ]
    desc = T.concat [ "Представитель: ", rep, "\\n", "Турнир: ", url]

workTournaments :: IO ()
workTournaments = do
  ets <- runRatingApi $ getAllItems tournaments
  case ets of
    Left err -> T.putStrLn $ T.pack $ show err
    Right ts -> do
      now <- getCurrentTime
      let fname = "tournaments.ics"
          oname = "online.ics"
      withFile fname WriteMode $ \h -> withFile oname WriteMode $ \onh -> do
        T.hPutStrLn h calStart
        T.hPutStrLn onh calStart
        forM ts $ \t -> do
          let TournamentId tid = trs_idtournament t
          T.putStr $ T.concat [ tid, " - ", trs_name t, "..." ]
          when ((trs_archive t == Just "0" ||
                addLocalTime (30 * 24 * 3600 :: NominalDiffTime) (trs_dateEnd t) > utcToLocalTime utc now) &&
                trs_typeName t /= Just TotalScore) $ do
            etl <- runRatingApi $ tournament $ trs_idtournament t
            case etl of
              Left err -> T.putStrLn $ T.concat [ "FAIL: ", T.pack $ show err ]
              Right [tl] -> do
                when (trn_town tl == Just "Онлайн" || "Онлайн:" `T.isPrefixOf` trn_name tl) $ do
                  T.putStr " online... "
                  T.hPutStrLn onh $ mkTournament now tl
                  T.hPutStrLn onh ""
                T.putStrLn "OK"
                T.hPutStrLn h $ mkTournament now tl
                T.hPutStrLn h ""
        T.hPutStrLn onh calEnd
        T.hPutStrLn h calEnd

mkTournament :: UTCTime -> Tournament -> Text
mkTournament now Tournament{..}
  = T.intercalate "\r\n"
    [ "BEGIN:VEVENT"
    , "UID:" `T.append` uid
    , "DTSTAMP:" `T.append` ts
    , "DTSTART;TZID=Europe/Moscow:" `T.append` start
    , "DTEND;TZID=Europe/Moscow:" `T.append` end
    , "SUMMARY:" `T.append` summary
    , "LOCATION:" `T.append` loc
    , "URL:" `T.append` url
    , "DESCRIPTION:" `T.append` desc
    , "OFFICIAL:" `T.append` trn_siteUrl
    , "END:VEVENT"
    ]
  where
    TournamentId tid = trn_idtournament
    uid = T.concat [ "t", tid, "@calendar.chgk.me" ]
    format :: FormatTime t => t -> Text
    format = T.pack . formatTime defaultTimeLocale "%Y%m%dT%H%M%S"
    ts = format now `T.append` "Z"
    start = format trn_dateStart
    end = format trn_dateEnd
    town = case trn_typeName of
      Nothing -> "(неизвестный - null)"
      Just Casual -> fromMaybe "(неизвестный город)" trn_town
      Just Regional -> fromMaybe "(неизвестный город)" trn_town
      Just TotalScore -> "(общий зачёт)"
      _ -> "(синхронный)"
    typeName = case trn_typeName of
      Nothing -> "Неизвестный - null"
      Just Casual -> "Турнир"
      Just Regional -> "Региональный"
      Just TotalScore -> "Общий зачёт"
      Just Synchronous -> "Синхрон"
      Just StrictlySynchronous -> "Строго синхронный"
      Just Asynchronous -> "Асинхрон"
      Just Marathon -> "Марафон"
      _ -> "Неизвестный"
    summary = T.concat $
      [ typeName, ": ", trn_name ]
    loc = town
    url = T.concat [ "https://rating.chgk.info/tournament/", tid ]
    desc = T.concat
      [ typeName, ": ", trn_longName, "\n"
      , "\tВзнос: ", fromMaybe "(NA)" trn_mainPaymentValue, " ", fromMaybe "(NA)" trn_mainPaymentCurrency, "\n"
      , "\tЛьготный взнос: ", fromMaybe "(NA)" trn_discountedPaymentValue, " ", fromMaybe "(NA)" trn_discountedPaymentCurrency, " - ", fromMaybe "(NA)" trn_discountedPaymentReason, "\n"
      , "\tТурнир: ", url, "\n"
      , "\tОфициальный сайт: ", trn_siteUrl
      ]

