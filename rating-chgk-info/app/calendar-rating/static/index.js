
var model = [];

function update() {
    var input = $("#town").val().toUpperCase();
    $("tr").attr("hidden","");
    model.filter(function(town){
        var other = town.other ? town.other : "";
        return town.name.toUpperCase().startsWith(input) || other.toUpperCase().startsWith(input);
    }).forEach(function(town){
        $("#t" + town.id).removeAttr("hidden");
    });
}

function load() {
    $.getJSON("names.json", function(data){
        model = data.sort(function(x,y) { return x.name.localeCompare(y.name); });
        console.log(model);
        $("#towns").empty();
        model.forEach(function(town){
            var t = "<td>" + town.name + "</td>";
            var o = "<td>" + (town.other ? town.other : "") + "</td>";
            var r = "<td>" + (town.region ? town.region : "") + "</td>";
            var c = "<td>" + (town.country ? town.country : "") + "</td>";
            var l = "<td><span class=\"px-md-1\"><a href=\"t" + town.id + ".ics\"><i class=\"fas fa-file-download\"></i></a></span><span class=\"px-md-1\"><a href=\"calendar.html?t=t" + town.id + "&mode=month\"><i class=\"far fa-calendar\"></i></a></span></td>";
            $("#towns").append("<tr id=\"t" + town.id + "\">" + t + o + r + c + l + "</tr>");
        });
    });
}

$(function(){
    load();
    $("#town").change(update);
    $("#town").keyup(update);
    $("#search").submit(function(){return false;});
})

