
-- Директива компилятора для использования Text в строковых литералах
{-# LANGUAGE OverloadedStrings #-}

-- Библиотеки
import RatingChgkInfo
import qualified Data.Text as T
import qualified Data.Text.IO as T

-- Точка входа
main :: IO ()
main = do
  -- Получаем список турниров
  Right ts <- runRatingApi $ getAllItems tournaments
  -- Проходимся по нему
  forM_ ts $ \TournamentShort{trs_idtournament = ident} -> do
    -- Поскольку здесь запускается отдельный runRatingApi, возможные ошибки
    -- одного турнира не приведут к остановке работы для следующих турниров, эти
    -- запуски независимы
    er <- runRatingApi $ do
      -- Выводим идентификатор турнира
      liftIO $ print ident
      -- Получаем список спорных и выводим их количество
      cs <- tournamentControversials ident
      liftIO $ T.putStrLn $ T.concat [ "Controversials ok: ", T.pack $ show $ length cs ]
      -- Получаем список апелляций и выводим их количество
      as <- tournamentAppeals ident
      liftIO $ T.putStrLn $ T.concat [ "Appeals ok: ", T.pack $ show $ length as ]
    -- Если удалось получить и спорные, и апелляции, выводим OK, иначе FAIL
    T.putStrLn $ either (const "FAIL") (const "OK") er

-- Ещё один пример: получаем новые спорные и апелляции на турнирах с 2016 по 2019 гг.
test2 :: IO ()
test2 = void $ runRatingApi $ do
  ts <- getAllItems tournaments
  let Just s2016   = readMaybe "2016-01-01 00:00:00"
      Just jan2019 = readMaybe "2019-01-11 00:00:00"
      ss = [ t
           | t <- ts
           , trs_typeName t `elem` map Just [Synchronous, StrictlySynchronous, Asynchronous]
           , trs_dateStart t >= s2016
           , trs_dateEnd t <= jan2019
           ]
  forM_ ss $ \TournamentShort{trs_idtournament = ident, trs_dateStart = start} -> do
    liftIO $ T.putStr "."
    cs <- tournamentControversials ident
    as <- tournamentAppeals ident
    let ncs = filter ((==ClaimNew) . conStatus) cs
        nas = filter ((==ClaimNew) . appStatus) as
    when (length ncs + length nas > 0) $ do
      liftIO $ T.putStrLn ""
      liftIO $ print ((ident, start), (length ncs, length cs), (length nas, length as))

