-- Наша библиотека
import RatingChgkInfo
-- И немного стандартных библиотек
import Control.Monad (forM, void)
import Control.Monad.IO.Class (liftIO)
import Data.List (nub)
import Data.Time (LocalTime(..),fromGregorian,midnight)
-- Точка входа в приложение
main :: IO ()
main = void $ runRatingApi $ do
  -- Получим список всех очных турниров
  --
  -- Функция tournaments получает одну страницу турниров, а функция getAllItems
  -- оборачивает подобные функции, чтобы пройтись по всем страницам.
  -- Далее из этого списка выбираются очные турниры 2018 года
  let s2018 = LocalTime (fromGregorian 2018 1 1) midnight
      e2018 = LocalTime (fromGregorian 2019 1 1) midnight
  tourns <- filter (\t -> trs_typeName t == Just Casual &&
                          trs_dateStart t >= s2018 &&
                          trs_dateEnd t <= e2018) <$>
            getAllItems tournaments
  -- Проходимся по полученному списку
  ts <- forM tourns $ \TournamentShort{trs_idtournament = ident} -> do
    -- Получаем результаты турнира с идентификатором ident
    res <- tournamentResults ident
    -- Возвращаем названия команд-участниц
    pure (map tr_current_name res)
  -- Выводим, сколько уникальных названий было по всем турнирам
  liftIO (print (length (nub ts)))

