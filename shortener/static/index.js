
function updateIdentity() {
    var name = localStorage.getItem('name');
    var mail = localStorage.getItem('mail');
    var roles = localStorage.getItem('roles');

    if (name) {
        $("#identity").html("<b>Name:</b> " + name + ". <b>Email:</b> " + mail + ". <b>Roles:</b> " + roles);
    } else {
        $("#identity").html("Авторизуйтесь!");
    }
}

function clearIdentity() {
    localStorage.removeItem('name');
    localStorage.removeItem('mail');
    localStorage.removeItem('token');
    localStorage.removeItem('roles');
    $('#identity').html("");
}

function updateLinks() {
    var token = localStorage.getItem('token');

    if (token) {
        $.ajax({
            url: "/u",
            type: "GET",
            dataType: "json",
            headers: { "Authorization": "Bearer " + token},
            success: function(d) {
                $("#list").empty();
                d.forEach(function(url) {
                    $("#list").append("<tr><td><tt>" + url[0] + "</tt></td><td>" + url[1] + "</td><td><a href=\"/u/" + url[0] + "\" target=\"_blank\">Go</a></td></tr>");
                });
            },
            error: function(err) {
                console.log("Error: ", err);
                if (err.status == 401) {
                    clearIdentity();
                }
            }
        });
    }
}

function newLink() {
    var token = localStorage.getItem('token');

    if (token) {
        $.ajax({
            url: "/u",
            data: "\"" + $("#long").val() + "\"",
            type: "POST",
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            headers: { "Authorization": "Bearer " + token },
            success: function (short) {
                console.log("Shortened: " + short);
                updateLinks();
            },
            error: function(err) {
                console.log("Error: " + err);
                if (err.status == 401) {
                    clearIdentity();
                    window.location='/auth';
                }
            }
        });
    } else {
        $("#identity").html("Авторизуйтесь!");
    }
    return false;
}

$(function(){
    $("#new").click(newLink);
    updateIdentity();
    updateLinks();
});

