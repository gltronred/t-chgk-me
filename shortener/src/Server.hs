{- |
Module      : Server
Description : Реализация API собственно приложения
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

В данном модуле находится реализация API собственно приложения. Описанием API собственно приложения является тип 'LinkApi'.

-}
{-# LANGUAGE OverloadedStrings #-}

module Server
  ( -- * Реализация сервера
    linkServer
  , listAllLinks
  , createLink
  , redirectLink
    -- * Вспомогательные функции
  , withAuthenticated
  , withUser
  ) where

import           Control.Monad.IO.Class
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Text.Encoding
import           Servant
import           Servant.Auth.Server

import           Api
import           Auth (getUserInfo)
import qualified DB
import           DB (withDb)
import           Types
import           Utils

-- | Проверка 'AuthResult' (туда после раскодирования и проверки попадает
-- информация из JWT-токена приложения, передаваемого в заголовке
-- @Authorization@).
--
-- Либо выбрасывает ошибки 401 (если пользователь не
-- авторизован), либо выполняет заданную функцию-обработчик с результатом
-- авторизации
withAuthenticated :: AuthResult a -- ^ Результат авторизации с информацией об
                                  -- @a@
                  -> (a -> Handler b) -- ^ Обработчик информации @a@, который
                                      -- возвращает @b@
                  -> Handler b        -- ^ Результат обработки
withAuthenticated (Authenticated a) handle = handle a
withAuthenticated _res _ = forbidden "Authorize at auth.chgk.me"

-- | Получение и проверка информации о пользователе и запуск функций со списком
-- ролей и пользователем.
--
-- Получает информацию о токене при помощи 'getUserInfo' и, если токен
-- действует, получает список ролей текущего пользователя.
--
-- Далее вызывает функцию-обработчик с этим (возможно, пустым) списком ролей и
-- текущим пользователем.
--
-- Если токен недействителен, возвращает код 401 с текстом ошибки.
withUser :: OIDCEnv             -- ^ Информация, необходимая для работы клиента
                                -- OpenID Connect
         -> ([Text] -> User -> Handler b) -- ^ Функция-обработчик, которая
                                          -- принимает список ролей и текущего
                                          -- пользователя
         -> User                          -- ^ Текущий пользователь
         -> Handler b                     -- ^ Результат обработки
withUser env handle user = do
  eroles <- liftIO $ getUserInfo env user
  case eroles of
    Left err -> note err >> forbidden (encodeUtf8 err)
    Right roles -> handle roles user

-- | Требует авторизации. Если текущий пользователь является админом в
-- приложении, возвращает все ссылки, иначе - только ссылки, созданные
-- пользователем
--
-- Использует функцию 'selectUrls' для работы с БД
listAllLinks :: OIDCEnv         -- ^ Информация, необходимая для работы клиента OpenID Connect
             -> Server ListAllLinks -- ^ Реализация получения списка ссылок
listAllLinks env@OIDCEnv{config=cfg} auth = withAuthenticated auth $ withUser env $ \roles user -> do
  note $ T.concat $ "Roles: " : roles
  liftIO $ withDb cfg $ DB.selectUrls $ if "admin" `elem` roles then Nothing else Just $ mail user

-- | Использует функцию 'insertUrl' для работы с БД
createLink :: OIDCEnv -- ^ Информация, необходимая для работы клиента OpenID Connect
           -> Server CreateLink -- ^ Реализация создания ссылки
createLink OIDCEnv{config=cfg} auth (Long long) = withAuthenticated auth $ \user ->
  liftIO $ withDb cfg $ DB.insertUrl user long

-- | В случае, если ссылка не найдена, возвращает ошибку 404
--
-- Использует функцию 'getUrl' для работы с БД
redirectLink :: OIDCEnv         -- ^ Информация, необходимая для работы клиента OpenID Connect
             -> Server RedirectLink -- ^ Реализация переадресации по короткой ссылке
redirectLink OIDCEnv{config=cfg} short = do
  mlong <- liftIO $ withDb cfg $ DB.getUrl short
  case mlong of
    Nothing -> throwError err404
    Just long -> redirect $ encodeUtf8 long

-- | Информация 'OIDCEnv' необходима для выполнения запросов о токене
-- пользователя внутри 'withUser'
linkServer :: OIDCEnv           -- ^ Информация, необходимая для работы клиента OpenID Connect
           -> Server LinkApi    -- ^ Реализация API собственно приложения
linkServer env
  =    listAllLinks env
  :<|> createLink env
  :<|> redirectLink env

