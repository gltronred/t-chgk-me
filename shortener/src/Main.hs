{- |
Module      : Main
Description : Основной модуль с точкой входа
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

Демо-страница использования сервера авторизации <https://auth.chgk.me>.

Используется язык haskell, но идеи должны быть понятными даже без знания языка.
Для других языков тоже есть библиотеки OpenId Connect:
<https://openid.net/developers/libraries/>.

Для понимания принципов работы должно быть достаточно этой документации. В
документации к типам функций есть ссылки на типы их аргументов, стоит
поглядывать и туда. Если интересна реализация, исходный код любой функции можно
увидеть по ссылке Source рядом с ней.

Для изучения работы с OpenID Connect стоит посмотреть модули:

  * "Api" с описанием API всего приложения и, в частности, описание API для
    авторизации в типе 'AuthApi';

  * "Auth", в котором подробно описано используемое подмножество OpenID Connect
    и имеющаяся реализация авторизации;

  * и, конечно, "Types", в котором описаны используемые во всём приложении типы.
    Для авторизации будут необходимы типы 'User' с информацией о пользователе и
    'OIDCEnv' с информацией, необходимой для работы клиента OpenID Connect.

Для изучения логики собственно приложения нужны модули:

  * "Types" с типами, используемыми во всём приложении. В частности, там
    вводятся типы 'Short' и 'Long', которые позволяют компилятору различать и
    проверять использование сокращённых ссылок, исходных ссылок и прочих
    текстовых данных;

  * "Api" с описанием API всего приложения и, в частности, тип 'LinkApi' с
    логикой собственно приложения;

  * "Server" с реализацией этого API;

  * "DB", в котором инкапсулирована работа с базой данных.

Всевозможные вспомогательные функции, используемые в нескольких модулях,
находятся в модуле "Utils".
-}

----------------------------------------------------------------------------------
-- Директивы компилятора и библиотеки

{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Main
  ( -- * Точка входа
    main
  , app
  , server
  , staticFiles
  ) where

import Api
import Auth
import Server
import Types

import Data.Aeson
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.RequestLogger
import Servant
import Servant.Auth.Server
import System.Exit

-- | Реализация API статических файлов. Выдаёт файлы из директории @static/@.
staticFiles :: Server HomepageApi
staticFiles = serveDirectoryWebApp "static/"

-- | Реализация тривиально собирается из реализаций API авторизации
-- 'authServer', реализации API приложения 'linkServer' и реализации API
-- статических файлов 'staticFiles'
server :: OIDCEnv               -- ^ Информация, необходимая для работы клиента OpenID Connect
       -> Server Api            -- ^ Реализация API всего приложения
server env
  =    authServer env
  :<|> linkServer env
  :<|> staticFiles

-- | Веб-приложение с реализацией API из функции 'server'
app :: OIDCEnv                  -- ^ Информация, необходимая для работы клиента OpenID Connect
    -> Application
app env = serveWithContext api context $ server env
  where context = cookieCfg :. jwtCfg :. EmptyContext
        cookieCfg = cookieSettings env
        jwtCfg = jwtSettings env

-- | Точка входа в выполняемый файл:
--
--  * читает и парсит файл @config.json@ с конфигурацией приложения 'Config';
--
--  * выполняет инициализацию информации, необходимой клиенту OpenID Connect;
--
--  * запускает веб-приложение на порту 4102 и начинает логирование всех
--    запросов в stdout.
main :: IO ()
main = do
  ecfg <- eitherDecodeFileStrict "config.json"
  case ecfg of
    Left err -> die $ "Can't parse config.json: " ++ err
    Right cfg -> do
      let jwtCfg = defaultJWTSettings $ jwk cfg
          cookieCfg = defaultCookieSettings
                      { cookieXsrfSetting = Just defaultXsrfCookieSettings
                        { xsrfExcludeGet = True}
                      }
      oidcEnv <- initializeOIDC cfg cookieCfg jwtCfg
      run 4102 $ logStdoutDev $ app oidcEnv

