{- |
Module      : Auth
Description : Реализация API для авторизации
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

В этом модуле реализован API для работы с авторизацией, который описывается в
'Api.AuthApi'.

= До разработки приложения

Для работы с сервером авторизации разработчику любого клиентского приложения
необходимо выполнить следующие организационные шаги:

 * обратиться к администраторам сервера авторизации и зарегистрировать своё
   приложение на сервере авторизации;

 * при регистрации нужно:

      - описать своё приложение и желаемое название (для записи информации,
        которая будет показываться конечным пользователям),
      - перечислить необходимые роли пользователей (этот список можно будет менять
        в дальнейшем),
      - указать тип приложения:

          [confidential]: клиенту требуется секрет для запуска процесса логина,
          [public]: клиенту не требуется секрет для запуска логина,
          [bearer-only]: клиент не запускает логин,

      - какие из следующих вариантов логина будут использоваться:

          * Authorization Code Flow,
          * Implicit Flow,

          * Direct Access Grants (клиент напрямую передаёт логин/пароль
            пользователя серверу авторизации),

          * Service Accounts (клиент может получить токен, связанный не с
            каким-то пользователем, а с клиентом),

      - корневой URL и допустимые URL для перенаправления, допустимые Web Origins
        для заголовков CORS;

 * после регистрации разработчик получает пару client-id и client-secret,
   которые может использовать для авторизации клиента на сервере авторизации.
   Допустимы и другие способы авторизации: подписанный (открытым ключом либо
   секретом клиента) JWT, клиентские X.509 сертификаты.

Например, для данного приложения информация при регистрации выглядела бы так:

 * __Название__: @s.chgk.me@.

 * __Описание__: @Сокращение ссылок - демо для сервера
   авторизации@.

 * __Роли__: @администратор@ (роль обычного пользователя и роль гостя есть у всех
   клиентов).

 * __Тип приложения__: @confidential@

 * __Варианты логина__: @Authorization Code Flow, Implicit Flow@.

 * __Корневой URL__: <https://s.chgk.me>.

 * __URL для перенаправления__:

     - @https:\/\/s.chgk.me\/auth\/*@
     - @http:\/\/localhost:4102\/auth\/*@

 * __Web Origins__: @https:\/\/s.chgk.me@.

= Процесс авторизации (в варианте Authorization Code Flow)

 * Клиент выполняет запрос к Authorization Endpoint (см. 'redirectAS')
 * AS идентифицирует пользователя и возвращает Authorization Code, выполняя вызов страницы-callback
 * Клиент выполняет запрос к Token Endpoint, указав Authorization Code, и получает Identity Token и Access Token (см. 'callbackAS')
 * На нашей странице callback мы просто сохраняем нужную информацию в виде (подписанного, но не зашифрованного) JWT-токена в local storage (см. 'cbPage'). По-хорошему, схема должна быть сложнее
 * Клиент может выполнить запрос к Token Introspection Endpoint, указав секрет клиента и Access Token, и получить информацию о пользователе, которой нет в Identity Token (см. 'getUserInfo')
 * В нашем случае при использовании API требуется передавать заголовок @Authorization@ с JWT Token

-}

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Auth
  ( -- * Получение информации о сервере авторизации
    redirectUri
  , initializeOIDC
    -- * Реализация авторизации в приложении
  , authServer
    -- ** Перенаправление на сервер авторизации
  , redirectAS
    -- ** Обработка callback-запроса
  , AuthInfo (..)
  , callbackAS
  , cbPage
    -- * Получение дополнительной информации о токене и пользователе
  , getUserInfo
  , verifyUserInfo
  ) where

import           Control.Lens
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.Foldable (toList)
import           Data.List (intersperse)
import           Data.Maybe (fromMaybe)
import           Data.Proxy
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Text.Encoding
import           Data.Time.Clock.POSIX
import           GHC.Generics
import           Network.HTTP.Client hiding (Proxy)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Network.HTTP.Types
import           Network.URI (parseURI, relativeTo)
import           Servant
import           Servant.Auth.Server
import           Text.Blaze.Html (Html)
import qualified Text.Blaze.Html5 as H
import qualified Web.OIDC.Client as O

import           Api (CallbackAS, RedirectAS, AuthApi, api)
import           Types (Config(..), OIDCEnv(..), User(..))
import           Utils (genRandomBS, redirect, note, forbidden)

-- | Для работы OpenID Connect необходимо предусмотреть запрос, который будет
-- выполнять сервер авторизации после попытки логина.
--
-- В нашем приложении мы генерируем адрес этой страницы в типобезопасном виде, и
-- он не рассинхронизируется с остальным API
--
-- В нашем случае адрес выглядит так: @https:\/\/s.chgk.me\/auth\/cb@
redirectUri :: Config           -- ^ Конфигурация приложения, чтобы получить базовый URL
            -> ByteString       -- ^ Адрес для callback-запросов
redirectUri Config{ base = b } = B.toStrict $ B.pack $ show $ (linkURI $ safeLink api cb Nothing Nothing) `relativeTo` baseUri
  where
    baseUri = fromMaybe (error "Wrong base URI") $ parseURI b
    cb = Proxy :: Proxy ("auth" :> CallbackAS)

-- | Для работы протокола необходимо знать разные endpoint на AS, такие как
-- Authorization Endpoint, Token Endpoint и т.п. Их значения можно заполнить
-- вручную, а можно всю эту информацию получить при помощи OpenID Connect
-- Discovery в виде JSON из
-- <https://auth.chgk.me/auth/realms/chgk/.well-known/openid-configuration>
--
-- Как правило, библиотекам достаточно указать часть до @.well-known@.
--
-- В нашем приложении мы выполняем этот процесс, инициализируем менеджер
-- соединений и сохраняем необходимую информацию в 'OIDCEnv'.
initializeOIDC :: Config        -- ^ Конфигурация приложения, чтобы при помощи
                                -- client-id, секрета и 'redirectUri'
                                -- инициализировать библиотеку
               -> CookieSettings -- ^ Настройки для Cookies, используемых при
                                 -- авторизации и для защиты от XSRF-атак
               -> JWTSettings    -- ^ Настройки для JWT-токенов, используемых
                                 -- при авторизации в приложении
               -> IO OIDCEnv     -- ^ Информация, необходимая для работы клиента
                                 -- OIDC
initializeOIDC cfg@Config{ clientId = i, clientSecret = s } cs jwts = do
  let ident  = encodeUtf8 i
      secret = encodeUtf8 s
      redir  = redirectUri cfg
  mgr <- newManager tlsManagerSettings
  prov <- O.discover "https://auth.chgk.me/auth/realms/chgk" mgr
  pure $ OIDCEnv
    { oidc = O.setCredentials ident secret redir $ O.newOIDC prov
    , manager = mgr
    , provider = prov
    , config = cfg
    , cookieSettings = cs
    , jwtSettings = jwts
    }

-- | Токен проверяется, как указано в п.3.1.3.7 спецификации OpenID Connect
--
-- В частности, проверяются поля @iss@, @aud@, @azp@, @exp@.
--
-- Кроме того, проверяется, что токен действует (поле @active@).
--
-- Если токен проходит проверку, то возвращается список ролей из поля
-- @[resource_access][s-chgk-me][roles]@.
verifyUserInfo :: O.IssuerLocation -- ^ Адрес сервера авторизации, должен совпадать с @iss@
               -> Text             -- ^ Идентификатор клиента, должен совпадать
                                   -- с @aud@ и @azp@
               -> POSIXTime        -- ^ Текущее время, должно быть раньше, чем
                                   -- момент времени @exp@
               -> Value            -- ^ Проверяемый токен
               -> Either Text [Text] -- ^ Сообщение об ошибке либо список ролей
verifyUserInfo issuer ident now v = do
  let grd e b = unless b $ Left e
      strKey k = v ^? key k . nonNull . _String
  grd "!active" $ v ^? key "active" . nonNull . _Bool == Just True
  grd "iss /= issuer (3.1.3.7.2)" $ strKey "iss" == Just issuer
  grd "aud /= clientId (3.1.3.7.3)" $ strKey "aud" == Just ident
  grd "azp /= clientId (3.1.3.7.5)" $ strKey "azp" == Just ident
  let exp = fromMaybe 0 (v ^? key "exp" . nonNull . _Integral) :: Int
  grd "exp >= now (3.1.3.7.9)" $ exp >= truncate (realToFrac now :: Double)
  -- grd "auth_time + 300 <= now (3.1.3.7.13)"
  pure $ toList $ v ^.. key "resource_access" . key "s-chgk-me" . key "roles" . values . _String

-- | Выполняется запрос к Token Introspection Endpoint, токен проверяется
-- функцией 'verifyUserInfo' и возвращается список ролей пользователя на клиенте
-- (или ошибка)
--
-- Для интроспекции нужно сделать POST-запрос к
-- <https://auth.chgk.me/auth/realms/chgk/protocol/openid-connect/token/introspect>.
--
-- Запрос должен быть защищён Basic Auth с именем пользователя - идентификатором
-- клиента и паролем - секретом клиента.
--
-- В теле запроса должен быть токен (переданный как форма, т.е.
-- @token=eyJhbG...z0a__w@)
getUserInfo :: OIDCEnv          -- ^ Информация для работы клиента OpenID Connect
            -> User             -- ^ Информация о пользователе (в ней сохранён
                                -- проверяемый токен в поле 'Types.token')
            -> IO (Either Text [Text]) -- ^ Текст ошибки, либо список ролей
                                       -- пользователя на клиенте
getUserInfo env usr = do
  let ident = clientId $ config env
      secret = clientSecret $ config env
      tok = token usr
      mgr = manager env
      as = O.issuer $ O.configuration $ provider env
      tokenEndpoint = O.tokenEndpoint $ O.configuration $ provider env
      introspectionUrl = T.concat [tokenEndpoint, "/introspect"]
      req = urlEncodedBody [("token", encodeUtf8 tok)] $
            applyBasicAuth (encodeUtf8 ident) (encodeUtf8 secret) $
            parseRequest_ $ T.unpack introspectionUrl
  resp <- httpLbs req mgr
  case statusCode $ responseStatus resp of
    200 -> case eitherDecode $ responseBody resp of
      Left err -> pure $ Left $ T.pack err
      Right res -> do
        now <- getPOSIXTime
        pure $ verifyUserInfo as ident now res
    _ -> pure $ Left $ T.concat ["Wrong code: ", T.pack $ show resp]

-- | Выполняется переадресация к Authorization Endpoint:
-- <https://auth.chgk.me/auth/realms/chgk/protocol/openid-connect/auth>
--
-- Запрашиваются требования (claim): openid, profile, email
--
-- В качестве состояния (state) используется случайная строка, сгенерированная
-- при помощи 'genRandomBS'.
redirectAS :: OIDCEnv           -- ^ Информация для работы клиента OpenID Connect
           -> Server RedirectAS -- ^ Реализация запроса на переадресацию к серверу авторизации
redirectAS env = do
  st <- liftIO genRandomBS
  uri <- O.getAuthenticationRequestUrl (oidc env) [O.openId, O.profile, O.email] (Just st) []
  redirect $ B.toStrict $ B.pack $ show uri

-- | Информация, возвращаемая в Identity Token
data AuthInfo = AuthInfo
  { email :: Text               -- ^ Почтовый адрес пользователя
  , email_verified :: Bool      -- ^ Верифицирован ли адрес
  , name :: Text                -- ^ Имя пользователя
  } deriving (Eq,Show,Read,Generic)

instance FromJSON AuthInfo
instance ToJSON AuthInfo

-- | Выполняются следующие действия:
--
--   * проверяется параметр @error@, если он есть - при логине произошла ошибка,
--     возвращаем 401;
--
--   * проверяется параметр @code@, если его нет - возвращаем 401;
--
--   * выполняется запрос к Token Endpoint
--     <https://auth.chgk.me/auth/realms/chgk/protocol/openid-connect/token>.
--     При запросе передаётся код авторизации. В ответ получаем
--     идентификационный токен и токен доступа;
--
--   * декодируется идентификационный токен, если это не удалось - возвращаем
--     401;
--
--   * проверяется, что почтовый адрес верифицирован, иначе - 401;
--
--   * создаётся JWT-токен с информацией о пользователе (см. 'User') и
--     возвращается HTML-страница, на которой этот JWT-токен сохраняется в
--     локальное хранилище и происходит переадресация в корень сайта.
callbackAS :: OIDCEnv           -- ^ Информация для работы клиента OpenID Connect
           -> Server CallbackAS -- ^ Реализация callback-запроса
callbackAS env merr mcode = do
  case merr of
    Just err -> note (T.concat ["Callback error: ", err]) >> forbidden (encodeUtf8 err)
    Nothing -> case encodeUtf8 <$> mcode of
      Nothing -> note "Callback error: no code" >> forbidden "No code param"
      Just code -> do
        tokens <- liftIO $ O.requestTokens (oidc env) code (manager env)
        let jwt = O.unJwt $ O.jwt $ O.idToken tokens
            tok = O.accessToken tokens
            eAuthInfo = O.decodeClaims jwt :: Either O.JwtError (O.JwtHeader, AuthInfo)
        case eAuthInfo of
          Left err -> note (T.concat ["JWT decode/check error: ", T.pack $ show err]) >> forbidden "JWT decode/check problem"
          Right (_, authInfo) ->
            if email_verified authInfo
            then do
              let usr = User (name authInfo) (email authInfo) tok
                  jwts = jwtSettings env
              note $ T.concat ["Login success: ", tok]
              eroles <- liftIO $ getUserInfo env usr
              let roles = either (const []) id eroles
              etoken <- liftIO $ makeJWT usr jwts Nothing
              case etoken of
                Left err -> note (T.concat ["makeJWT failed: ", T.pack $ show err]) >> forbidden "Can't create token"
                Right t -> pure $ cbPage (decodeUtf8 $ B.toStrict t) usr roles
            else note "Unverified email" >> forbidden "Please, verify your email"

-- | Реализация API для авторизации состоит из двух частей:
--
--  * реализации запроса переадресации 'redirectAS';
--  * реализации callback-запроса 'callbackAS'.
authServer :: OIDCEnv           -- ^ Информация, необходимая для работы клиента OpenID Connect
           -> Server AuthApi    -- ^ Реализация API для авторизации
authServer env = redirectAS env :<|> callbackAS env

-- | Сохраняются следующие ключи в локальное хранилище:
--
--     [@token@] JWT-токен с информацией о пользователе;
--     [@name@]  имя пользователя;
--     [@mail@]  электронная почта пользователя;
--     [@roles@] список ролей через запятую.
cbPage :: Text                  -- ^ JWT-токен для авторизации
       -> User                  -- ^ Информация о пользователе
       -> [Text]                -- ^ Список ролей пользователя
       -> Html                  -- ^ Страница, которая сохраняет информацию и
                                -- перенаправляет в корень
cbPage tok user roles = H.docTypeHtml $ H.body $ do
  H.h1 "Logged in"
  H.script $ H.toHtml $ mconcat
    [ "localStorage.setItem('token','", tok, "');"
    , "localStorage.setItem('name','", username user, "');"
    , "localStorage.setItem('mail','", mail user, "');"
    , "localStorage.setItem('roles','", T.concat (intersperse ", " roles), "');"
    , "window.location='/index.html';"
    ]

