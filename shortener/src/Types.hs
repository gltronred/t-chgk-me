{- |
Module      : Types
Description : Типы, общие для всего приложения
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

В этом модуле описаны типы, которые используются в нескольких других модулях.

-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Types
  ( -- * Конфигурация приложения
    Config (..)
    -- * Типы для авторизации
  , OIDCEnv (..)
  , User (..)
    -- * Типы для логики приложения
  , Short (..)
  , Long (..)
  ) where

import Crypto.JOSE.JWK
import Data.Aeson
import Data.Text (Text)
import GHC.Generics
import Network.HTTP.Client (Manager)
import Servant.Auth.Server (FromJWT, ToJWT, CookieSettings, JWTSettings)
import Web.HttpApiData
import Web.OIDC.Client (OIDC, Provider)

-- | Сокращённая ссылка
newtype Short = Short
  { unShort :: Text             -- ^ Текст сокращённой ссылки (то, что идёт после @https:\/\/s.chgk.me\/u\/@)
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,FromHttpApiData)

-- | Исходная «длинная» ссылка
newtype Long = Long
  { unLong :: Text              -- ^ Полный текст исходной ссылки
  } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON,FromHttpApiData)

-- | Информация о пользователе
data User = User
  { username :: Text            -- ^ Имя пользователя
  , mail :: Text                -- ^ Электронная почта пользователя
  , token :: Text               -- ^ Токен доступа (access token), выдаваемый сервером авторизации
  } deriving (Eq,Show,Read,Generic)
instance FromJSON User
instance ToJSON User

instance FromJWT User
instance ToJWT User

-- | Конфигурация приложения
--
-- Для работы с OpenID Connect нужны client id и client secret. Возможны также
-- другие схемы аутентификации клиента (JWT; сертификаты X.509; JWT, подписанный
-- секретом клиента) на сервере авторизации (AS), но этот способ проще всего для
-- понимания.
--
-- Разумеется, в реальном коде эти данные не должны храниться в коде, поэтому мы
-- храним их в файле конфигурации.
--
-- Также понадобится redirect uri, на который AS будет перенаправлять после
-- логина. Возможные варианты redirect uri (с вайлдкардами) должны быть
-- настроены для каждого клиента на AS. Мы используем поле 'base' в функции
-- 'Auth.redirectUri' для генерации этого URI.
--
-- Кроме того, конфигурация содержит ключ 'jwk' для подписи собственных токенов
-- приложения, которые выдаются пользователям после авторизации и которые
-- содержат информацию о пользователе 'User'.
--
-- Более подробная информация о процессе авторизации находится в модуле "Auth".
--
-- Кроме этого, в конфигурации записана информация об используемой БД. Работа с
-- БД происходит в модуле "DB".
--
-- Пример файла конфигурации
--
-- @
--   {
--     "connStr": "demo.sqlite",
--     "base": "https://s.chgk.me",
--     "clientId": "s-chgk-me",
--     "clientSecret": "41bc...cedf",
--     "jwk": {"k": "NDFiY2...ZWRm", "kty":"oct"}
--   }
-- @
data Config = Config
  { connStr :: Text             -- ^ Строка соединения с БД. По сути - имя файла Sqlite
  , base :: String              -- ^ Базовый URL
  , clientId :: Text            -- ^ Идентификатор клиента
  , clientSecret :: Text        -- ^ Секрет клиента
  , jwk :: JWK                  -- ^ Ключ для подписи JWT-токенов приложения
  } deriving (Eq,Show,Generic)

instance FromJSON Config

-- | Информация, необходимая для работы клиента OpenID Connect. Создаётся при помощи функции 'Auth.initializeOIDC' из конфигурации 'Config'
data OIDCEnv = OIDCEnv
  { oidc :: OIDC                -- ^ Клиент OpenID Connect
  , manager :: Manager          -- ^ Менеджер соединений
  , provider :: Provider        -- ^ Информация о сервере авторизации
  , config :: Config            -- ^ Исходная конфигурация
  , cookieSettings :: CookieSettings -- ^ Настройки Cookies для авторизации в приложении
  , jwtSettings :: JWTSettings       -- ^ Настройки JWT-токенов для авторизации в приложении
  }

