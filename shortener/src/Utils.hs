{- |
Module      : Utils
Description : Различные вспомогальные функции, не вошедшие в остальные модули
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

В этом модуле собраны функции, которые не вошли в остальные модули

-}
{-# LANGUAGE OverloadedStrings #-}

module Utils
  ( -- * Коды ошибок HTTP
    redirect
  , forbidden
  , implementMe
    -- * Логирование
  , note
    -- * Случайные строки
  , genRandomBS
  ) where

import           Control.Monad (forM, replicateM)
import           Control.Monad.IO.Class (liftIO)
import           Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.List (intersperse)
import           Data.Text (Text)
import qualified Data.Text.IO as T
import           Servant
import           System.Random (randomRIO)

-- | Код 500 с ошибкой "Implement me!"
implementMe :: Handler a
implementMe = throwError $ err500 { errBody = "Implement me!" }

-- | Переадресация с кодом 302 на заданный URL
redirect :: ByteString          -- ^ Заданный URL
         -> Handler a
redirect loc = throwError $ err302 { errHeaders = [("Location", loc)] }

-- | Ошибка @401 Unauthorized@ с указанием причины в теле ответа
--
-- Устанавливает заголовок @WWW-Authenticate@
forbidden :: ByteString         -- ^ Указываемая в теле ответа причина
          -> Handler a
forbidden reason = throwError $ err401 { errHeaders = [("WWW-Authenticate", "Bearer realm=auth.chgk.me")]
                                       , errBody = B.fromStrict reason
                                       }

-- | Логирование произвольного текста
note :: Text                    -- ^ Текст
     -> Handler ()
note = liftIO . T.putStrLn

-- | Генерация случайной строки в формате
-- @uuuuuuuu-vvvv-wwww-xxxx-yyyyyyyyyyyy@. Строка может содержать строчные и
-- заглавные буквы латинского алфавита и цифры
genRandomBS :: IO ByteString
genRandomBS = mconcat . intersperse "-" <$> forM [8,4,4,4,12] gen
  where
    letters = ['A'..'Z'] <> ['0'..'9'] <> ['a'..'z']
    n = length letters
    toChar i = letters !! i
    gen m = fmap (B.toStrict . B.pack . map toChar) $
            replicateM m $
            randomRIO (0,n-1)

