{- |
Module      : DB
Description : Работа с базой данных
Copyright   : (c) Mansur Ziiatdinov, 2018-2019
License     : BSD-3
Maintainer  : chgk@pm.me
Stability   : experimental
Portability : POSIX

В этом модуле находятся функции для работы с базой данных. Поскольку приложение
демонстрационное, используется Sqlite. Из модуля экспортируются только функции,
которые работают с типами предметной области, все внутренности БД
инкапсулированы в модуле.

Строка для соединения с БД задаётся в конфигурации.

БД имеет следующую схему. В БД есть единственная таблица @url@, созданная при помощи следующего запроса:

@
  CREATE TABLE IF NOT EXISTS "url"
    ( "id" INTEGER PRIMARY KEY
    , "author" VARCHAR NOT NULL
    , "short" VARCHAR NOT NULL
    , "long" VARCHAR NOT NULL
    , "created_at" TIMESTAMP NOT NULL
    , CONSTRAINT "unique_short" UNIQUE ("short")
    );
@

Смысл полей:

  [@id@]     первичный ключ
  [@author@] электронная почта создателя ссылки
  [@short@]  сокращённая ссылка (часть после @https:\/\/s.chgk.me\/u\/@)
  [@long@]   куда ведёт ссылка
  [@created_at@] дата-время создания ссылки

-}

{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module DB
  ( -- * Соединение с БД
    withDb
    -- * Работа с БД
  , insertUrl
  , selectUrls
  , getUrl
  ) where

import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Maybe (maybe)
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           Database.Persist
import           Database.Persist.Sqlite
import           Database.Persist.TH
import           System.Random

import           Types

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Url
  author Text
  short Text
  long Text
  createdAt UTCTime
  UniqueShort short
  deriving Show
|]

-- | Запускает действия, изменяющие БД внутри монады IO. При каждом вызове БД
-- открывается заново
withDb :: Config                -- ^ Настройки приложения
       -> SqlPersistM a         -- ^ Действие с БД, возвращающее результат
       -> IO a                  -- ^ Результат запуска действия
withDb cfg act = runSqlite (connStr cfg) $ do
  runMigration migrateAll
  act

-- | Сокращает заданный URL и записывает результат в БД. Для сокращения просто генерируется случайная строка из 8 латинских букв.
insertUrl :: User               -- ^ Пользователь, который сокращает ссылку
          -> Text               -- ^ Исходная ссылка
          -> SqlPersistM Short  -- ^ Результат сокращения
insertUrl User{mail=m} long = do
  let letters = ['a'..'z']
      gen = randomRIO (0, length letters - 1)
      toChar i = letters !! i
  s@(Short short) <- liftIO $ Short . T.pack . map toChar <$> replicateM 8 gen
  now <- liftIO getCurrentTime
  _ <- insert $ Url m short long now
  pure s

-- | В случае, если задана электронная почта, возвращает список ссылок,
-- сокращённых этим пользователем. Если почта не задана, возвращает список всех
-- ссылок. Результирующий список отсортирован по убыванию времени создания
selectUrls :: Maybe Text        -- ^ Электронная почта, либо 'Nothing'
           -> SqlPersistM [(Short,Long)] -- ^ Список пар «сокращение - исходная ссылка»
selectUrls mmail = fmap (fmap toTuple) $ selectList cond [Desc UrlCreatedAt]
  where
    cond = maybe [] (\email -> [ UrlAuthor ==. email ]) mmail
    toTuple (Entity _ Url{urlShort=s,urlLong=l}) = (Short s, Long l)

-- | Если сокращённой ссылке не соответствует ни одна исходная, возвращается 'Nothing'
getUrl :: Short                 -- ^ Сокращённая ссылка
       -> SqlPersistM (Maybe Text) -- ^ Соответствующая исходная ссылка, либо 'Nothing'
getUrl = fmap (fmap $ urlLong . entityVal) .
         getBy . UniqueShort . unShort

