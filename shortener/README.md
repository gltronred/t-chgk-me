# shortener

[![Hackage](https://img.shields.io/hackage/v/shortener.svg)](https://hackage.haskell.org/package/shortener)
[![BSD3 license](https://img.shields.io/badge/license-BSD3-blue.svg)](LICENSE)

URL shortener for chgk.me - demo usage of authorization server