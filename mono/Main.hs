{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

import Prelude hiding (head)

import RatingChgkInfo
import RatingChgkInfo.Types.Unsafe

import Data.List (head, groupBy, nub, lookup, (!!))
import qualified Data.Text as T
import qualified Data.Text.IO as T

data Rep1 = Rep1
  { tourn :: TournamentId
  , totalTeams :: Int
  , totalReqs :: Int
  , monoReqs :: Int
  } deriving (Eq,Show,Read)

data Rep2 = Rep2
  { team :: TeamId
  , name :: Text
  , town :: Text
  , games :: Int
  , monoGames :: Int
  } deriving (Eq,Show,Read)

type InfoItem = (TournamentId, [Request])

getInfo :: TournamentId -> IO InfoItem
getInfo tid = do
  ers <- requests tid
  case ers of
    Left err -> do
      putStrLn $ "Error: " ++ show err
      pure (tid, [])
    Right rs -> pure (tid, rs)

collectInfo :: IO [InfoItem]
collectInfo = do
  ts <- map TournamentId . T.lines <$> T.readFile "/tmp/tourns"
  forM ts $ \t -> do
    print t
    getInfo t

loadInfos :: FilePath -> IO (Maybe [InfoItem])
loadInfos = fmap readMaybe . readFile


rep1 :: [InfoItem] -> IO [Rep1]
rep1 = mapM $ \(tid, rs) -> do
  let tlens = map (length . reqTeams) rs
  pure $ Rep1 tid (sum tlens) (length tlens) (length $ filter (==1) tlens)

infoToTeamReq :: InfoItem -> [((TournamentId, (Int,Text), Int), TeamName)]
infoToTeamReq (tid, rs) = concatMap (\r -> map ((tid, getRepresentative r, cnt r), ) $ reqTeams r) rs
  where getRepresentative r = (reqRepresentativeId r, reqRepresentativeFullname r)
        cnt r = length $ reqTeams r

toRep2 :: [(TeamName, (TournamentId, (Int,Text), Int))] -> Rep2
toRep2 grp = Rep2 ident n t (length grp) (length $ filter (\(_,(_tid,_rep,cnt)) -> cnt == 1) grp)
  where (tn, _) = head grp
        ident = TeamId $ T.pack $ show $ tnTeamId tn
        n = tnBaseName tn
        t = tnBaseTown tn

rep2 :: [InfoItem] -> IO [Rep2]
rep2 infos = do
  let teamReqs = concatMap infoToTeamReq infos
      getTeamId = tnTeamId . fst
      groupedByTeam = groupBy ((==)`on`getTeamId) $
                      sortBy (compare`on`getTeamId) $
                      map swap teamReqs
  pure $ map toRep2 groupedByTeam

rep1ToCsv :: Rep1 -> Text
rep1ToCsv rep1 = T.concat
  [ unTournamentId $ tourn rep1
  , ";"
  , T.pack $ show $ totalTeams rep1
  , ";"
  , T.pack $ show $ totalReqs rep1
  , ";"
  , T.pack $ show $ monoReqs rep1
  ]

rep1Head = "tournament-id;total-teams;total-reqs;mono-reqs"

rep1Csv :: [Rep1] -> Text
rep1Csv reps = T.unlines $ rep1Head : map rep1ToCsv reps


data ReqEdge = ReqEdge
  { reRep :: Text
  , reNar :: Text
  , reCnt :: Int
  } deriving (Eq,Show,Read)

toReqGraph :: (Request -> Bool) -> [InfoItem] -> [ReqEdge]
toReqGraph flt infos = let
  reqs = filter flt $ concatMap snd infos
  repNar r = (reqRepresentativeFullname r, reqNarratorFullname r)
  toReqEdge rs@(Request { reqRepresentativeFullname = rep
                        , reqNarratorFullname = nar
                        } : _) = ReqEdge rep nar (length rs)
  in map toReqEdge $
     groupBy ((==)`on`repNar) $
     sortBy (compare`on`repNar) reqs

getNodeCntIds :: [ReqEdge] -> [((Text,Int),Int)]
getNodeCntIds g = let
  withCnt f re = (f re, reCnt re)
  nodes = map (\ns@(n:_) -> (fst n, sum $ map snd ns)) $
          groupBy ((==)`on`fst) $
          sort $
          map (withCnt $ \r -> T.append "R " $ reRep r) g ++
          map (withCnt $ \r -> T.append "N " $ reNar r) g
  in zip nodes [1..]

printDot :: FilePath -> [ReqEdge] -> IO ()
printDot f g = withFile f WriteMode $ \h -> do
  let tpl = T.hPutStrLn h
  tpl "digraph g1 {"
  let nodeCntIds = getNodeCntIds g
  nodeIds <- fmap catMaybes $ forM nodeCntIds $ \((name,cnt),id) ->
    -- if cnt < 6
    -- then pure Nothing
    -- else do
    do
      tpl $ T.concat
        [ "n", T.pack $ show id
        , " ["
        , "label=\"", name, "/", T.pack $ show cnt, "\"", ","
        , "fillcolor="
        , if T.take 1 name == "N"
          then "green"
          else "red"
        , ","
        , "style=filled"
        , "];" ]
      pure $ Just (name, id)
  let cnts = map reCnt g
      n = length cnts
      med = cnts !! (n`div`2)
  forM_ g $ \(ReqEdge r n c) -> do
    case (lookup (T.append "R " r) nodeIds, lookup (T.append "N " n) nodeIds) of
      (Nothing, _) -> pure ()
      (Just rid, Nothing) -> pure ()
      (Just rid, Just nid) -> do
        let nodeName id = T.concat [ "n", T.pack $ show id ]
            style | c < med = "dotted"
                  | c ==med = "solid"
                  | c > med = "bold"
        tpl $ T.concat [ nodeName rid
                       , " -> "
                       , nodeName nid
                       , " [style=", style, "];"
                       ]
  tpl "}"

main :: IO ()
main = putStrLn "Hello!"

