# Центральный турнирный сайт t.chgk.me

Ввод и хранение всей информации, относящейся к турнирам

# Документация

Принципы, на которых основан API, описаны [здесь](/doc/api-principles.org).

# Сборка и установка

Для сборки и установки следует выполнить

```
cabal new-build all
```

При указании флага symlink-bindir установка будет произведена в указанный каталог:

```
cabal new-install --symlink-bindir=~/.cabal/bin
```

# Проекты в репозитории

* Проект t-chgk-me - центральный турнирный сайт
* Проект rating-chgk-me - библиотека работы с данными сайта рейтинга
* Проект shortener - сокращатель ссылок и демо-приложение для сервера авторизации

